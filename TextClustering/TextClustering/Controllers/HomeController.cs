﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using TextClustering.Models;
using TextClustering.Services;

namespace TextClustering.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult ClusterDocument(FileUploadModel model)
        {

            DocumentCollectionModel documentCollectionModel = new DocumentCollectionModel();
            documentCollectionModel.DocumentList = new List<string>();
            List<IFormFile> fileName = model.fileName;
            if (model.k <= 1)
            {
                ErrorResponse er = new ErrorResponse();
                er.IsErrror = true;
                er.Message = "Value of k must be greater than 1";
                TempData["response"] = JsonConvert.SerializeObject(er);
                return RedirectToAction("Index");
            }
            if (fileName == null)
            {
                ErrorResponse er = new ErrorResponse();
                er.IsErrror = true;
                er.Message = "File not selected";
                TempData["response"] = JsonConvert.SerializeObject(er);
                return RedirectToAction("Index");
            }
            if (fileName.Count < model.k)
            {
                ErrorResponse er = new ErrorResponse();
                er.IsErrror = true;
                er.Message = "Value of k can not be more than number of files";
                TempData["response"] = JsonConvert.SerializeObject(er);
                return RedirectToAction("Index");
            }
            foreach (IFormFile file in fileName)
            {
                if (file == null || file.Length == 0)
                {
                    ErrorResponse er = new ErrorResponse();
                    er.IsErrror = true;
                    er.Message = "File not selected";
                    TempData["response"] = JsonConvert.SerializeObject(er);
                    return RedirectToAction("Index");
                }
                string fName = file.FileName;
                if (!(fName.Substring(fName.Length - 4) == ".txt"))
                {
                    ErrorResponse er = new ErrorResponse();
                    er.IsErrror = true;
                    er.Message = "All files should be in (.txt) format";
                    TempData["response"] = JsonConvert.SerializeObject(er);
                    return RedirectToAction("Index");
                }

                var path = Path.Combine(
                            Directory.GetCurrentDirectory(), "wwwroot",
                            file.FileName);

                using (var stream = new FileStream(path, FileMode.Create))
                {
                    file.CopyTo(stream);
                }
                string textInFile = System.IO.File.ReadAllText(path);
                documentCollectionModel.DocumentList.Add(textInFile);
            }

            OutputModel output = new OutputModel();
            // this part is for clustering 
            List<DocumentVectorSpaceModel> dataInVectorSpace = VectorSpacing.toVectorSpace(documentCollectionModel);


            // this part is for k medoid algorithm
            int _counterKmedoid = 0;
            float _totalDistanceKmedoid = 0.0f;
            var watchKmedoid = new System.Diagnostics.Stopwatch();
            watchKmedoid.Start();
            List<DocumentObjModel> clusterKmedoid = KMedoid.DocumentClustering(model.k, dataInVectorSpace, ref _counterKmedoid, ref _totalDistanceKmedoid);
            watchKmedoid.Stop();
            output.watchKmedoid = watchKmedoid.ElapsedMilliseconds;
            output.totalDistanceKmedoid = _totalDistanceKmedoid;


            // this part is for k mean 
            int _counterKmean = 0;
            float totalMean = 0.0f;
            var watchKmean = new System.Diagnostics.Stopwatch();
            watchKmean.Start();
            List<DocumentObjModel> clusterKmean = KMean.PrepareDocumentCluster(model.k, dataInVectorSpace, ref _counterKmean, ref totalMean);
            watchKmean.Stop();
            int count = _counterKmean;
            output.totalMeanKmean = totalMean;
            output.watchKmean = watchKmean.ElapsedMilliseconds;
            output.KmedoidCluster = clusterKmedoid;
            output.KmeanCluster = clusterKmean;
            return View(output);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
