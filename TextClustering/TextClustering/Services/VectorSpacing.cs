﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TextClustering.Models;

namespace TextClustering.Services
{
    public class VectorSpacing
    {
        //To represent distinct word in document
        //Used for tokenization
        public static HashSet<string> distinctTerms;

        //Regular expression to split text
        private static Regex r = new Regex("([ \\t{}()\",:;. \n])");

        /// <summary>
        /// Convert text into vector space model
        /// </summary>
        /// <param name="collection"></param>
        /// <returns></returns>
        public static List<DocumentVectorSpaceModel> toVectorSpace(DocumentCollectionModel collection)
        {
            distinctTerms = new HashSet<string>();

            //Adding words in distinctTerms that are not stop words in english language           
            foreach (string documentContent in collection.DocumentList)
            {
                foreach (string term in r.Split(documentContent))
                {
                    if (!WordsHandler.IsStotpWord(term) && !WordsHandler.IsSymbol(term))
                        distinctTerms.Add(term);
                    else
                        continue;
                }
            }

            List<DocumentVectorSpaceModel> dataInVectorSpace = new List<DocumentVectorSpaceModel>();
            float[] vectorSpace;
            foreach (string document in collection.DocumentList)
            {
                int count = 0;
                vectorSpace = new float[distinctTerms.Count];
                foreach (string term in distinctTerms)
                {
                    vectorSpace[count] = FindWeight(collection.DocumentList, document, term);
                    count++;
                }
                var _documentVector = new DocumentVectorSpaceModel();
                _documentVector.Content = document;
                _documentVector.VectorSpace = vectorSpace;
                dataInVectorSpace.Add(_documentVector);
            }
            return (dataInVectorSpace);
        }

        private static float FindWeight(List<string> documentList, string document, string term)
        {
            float tf = FindTermFrequency(document, term);
            float idf = FindInverseDocumentFrequency(documentList, term);
            return tf * idf;
        }

        private static float FindTermFrequency(string document, string term)
        {
            int count = r.Split(document).Where(s => s.ToUpper() == term.ToUpper()).Count();
            //ratio of no of occurance of term t in document d to the total no of terms in the document
            return (float)((float)count / (float)(r.Split(document).Count()));
        }
        private static float FindInverseDocumentFrequency(List<string> documentList, string term)
        {
            //find the  no. of document that contains the term in whole document collection
            int count = documentList.ToArray().Where(s => r.Split(s.ToUpper()).ToArray().Contains(term.ToUpper())).Count();
            /*
             * log of the ratio of  total no of document in the collection to the no. of document containing the term
             * we can also use Math.Log(count/(1+documentCollection.Count)) to deal with divide by zero case; 
             */
            return (float)Math.Log((float)documentList.Count() / (float)count);
        }
    }
}
