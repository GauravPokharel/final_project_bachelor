﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TextClustering.Models;

namespace TextClustering.Services
{
    public class KMedoid
    {
        private static List<DocumentVectorSpaceModel> medoidCollection = new List<DocumentVectorSpaceModel>();
        private static List<DocumentObjModel> finalCluster;
        private static float totalCost = 0.0F;

        /// <summary>
        /// Prepares the document cluster, Grouping of similar 
        /// type of text document is done here
        /// </summary>
        /// <param name="k">initial cluster center</param>
        /// <param name="documentCollection">document corpus</param>
        /// <returns></returns>


        public static List<DocumentObjModel> DocumentClustering(int k, List<DocumentVectorSpaceModel> documentCollection, ref int _counter, ref float _totalDistanceKmedoid)
        {
            int loopBasedOnDoc = documentCollection.Count / k;
            int maxNoOfIteration = 15;
            int counter = 1;
            //prepares k initial centroid and assign one object randomly to each centroid
            List<DocumentObjModel> currentMedoidsCollection = new List<DocumentObjModel>();
            DocumentObjModel c;
            /*
             * Avoid repeation of random number, if same no is generated more than once same document is added to the next cluster 
             * so avoid it using HasSet collection
             */
            HashSet<int> uniqRand = new HashSet<int>();
            GenerateRandomNumber(ref uniqRand, k, documentCollection.Count);

            foreach (int pos in uniqRand)
            {
                c = new DocumentObjModel();
                c.GroupedDocument = new List<DocumentVectorSpaceModel>();
                c.GroupedDocument.Add(documentCollection[pos]);
                currentMedoidsCollection.Add(c);

                DocumentVectorSpaceModel medoid = documentCollection[pos];

                //Initial list of medoids
                medoidCollection.Add(medoid);
            }

            Boolean stoppingCriteria = false;
            List<DocumentObjModel> clusterSet;

            InitializeClusterCentroid(out clusterSet, currentMedoidsCollection.Count);
            float cost = 0.0F;

            foreach (DocumentVectorSpaceModel obj in documentCollection)
            {
                int index = FindClosestClusterCenter(currentMedoidsCollection, obj, out cost);
                clusterSet[index].GroupedDocument.Add(obj);
                totalCost += cost;
            }
            finalCluster = clusterSet;
            do
            {
                InitializeClusterCentroid(out currentMedoidsCollection, currentMedoidsCollection.Count());

                //new medoids are obtained
                currentMedoidsCollection = getNewMedoids(ref uniqRand, k, documentCollection);
                InitializeClusterCentroid(out clusterSet, currentMedoidsCollection.Count);
                float totalCostOfNewCluster = 0.0F;

                foreach (DocumentVectorSpaceModel obj in documentCollection)
                {
                    int index = FindClosestClusterCenter(currentMedoidsCollection, obj, out cost);
                    clusterSet[index].GroupedDocument.Add(obj);
                    totalCostOfNewCluster += cost;
                }
                if (totalCost > totalCostOfNewCluster)
                {
                    finalCluster = clusterSet;
                    totalCost = totalCostOfNewCluster;
                }
                else if (totalCost == totalCostOfNewCluster)
                {
                    stoppingCriteria = true;
                }
                _totalDistanceKmedoid = totalCost;
                counter++;
                //finalCluster.TotalDistance = totalCost;

            } while (stoppingCriteria == false && counter <= maxNoOfIteration && loopBasedOnDoc != counter);

            _counter = counter;
            return finalCluster;
        }

        /// <summary>
        /// Generates unique random numbers and also ensures the generated random number 
        /// lies with in a range of total no. of document
        /// </summary>
        /// <param name="uniqRand"></param>
        /// <param name="k"></param>
        /// <param name="docCount"></param>

        private static void GenerateRandomNumber(ref HashSet<int> uniqRand, int k, int docCount)
        {

            Random r = new Random();

            if (k > docCount)
            {
                do
                {
                    int pos = r.Next(0, docCount);
                    uniqRand.Add(pos);

                } while (uniqRand.Count != docCount);
            }
            else
            {
                do
                {
                    int pos = r.Next(0, docCount);
                    uniqRand.Add(pos);

                } while (uniqRand.Count != k);
            }
        }

        /// <summary>
        /// Initialize the result cluster centroid for the next iteration, that holds the result to be returned
        /// </summary>
        /// <param name="centroid"></param>
        /// <param name="count"></param>
        private static void InitializeClusterCentroid(out List<DocumentObjModel> centroid, int count)
        {
            DocumentObjModel c;
            centroid = new List<DocumentObjModel>();
            for (int i = 0; i < count; i++)
            {
                c = new DocumentObjModel();
                c.GroupedDocument = new List<DocumentVectorSpaceModel>();
                centroid.Add(c);
            }

        }


        //returns index of closest cluster medoid
        private static int FindClosestClusterCenter(List<DocumentObjModel> clusterCenter, DocumentVectorSpaceModel obj, out float cost)
        {

            float[] distanceMeasure = new float[clusterCenter.Count()];

            for (int i = 0; i < clusterCenter.Count(); i++)
            {

                distanceMeasure[i] = DistanceCalculation.FindCosineDistance(clusterCenter[i].GroupedDocument[0].VectorSpace, obj.VectorSpace);

            }
            cost = 0.0F;
            int index = 0;
            float minValue = distanceMeasure[0];
            for (int i = 0; i < distanceMeasure.Count(); i++)
            {
                if (distanceMeasure[i] < minValue)
                {
                    minValue = distanceMeasure[i];
                    index = i;
                }
                cost = minValue;
            }
            return index;
        }

        private static List<DocumentObjModel> getNewMedoids(ref HashSet<int> uniqRand, int k, List<DocumentVectorSpaceModel> documentCollection)
        {
            uniqRand.Clear();
            int docCount = documentCollection.Count();
            Random r = new Random();
            bool flag;

            if (k > docCount)
            {
                do
                {
                    flag = true;
                    int pos = r.Next(0, docCount);

                    foreach (var item in medoidCollection)
                    {
                        if (item == documentCollection[pos])
                        {
                            flag = false;
                        }
                    }
                    if (flag == true)
                    {
                        uniqRand.Add(pos);
                    }

                } while (uniqRand.Count != docCount);
            }
            else
            {
                do
                {
                    flag = true;
                    int pos = r.Next(0, docCount);
                    foreach (var item in medoidCollection)
                    {
                        if (item == documentCollection[pos])
                        {
                            flag = false;
                            break;
                        }
                    }
                    if (flag == true)
                    {
                        uniqRand.Add(pos);
                    }

                } while (uniqRand.Count != k);
            }
            List<DocumentObjModel> newMedoids = new List<DocumentObjModel>();
            foreach (int pos in uniqRand)
            {
                DocumentObjModel c = new DocumentObjModel();
                c.GroupedDocument = new List<DocumentVectorSpaceModel>();
                c.GroupedDocument.Add(documentCollection[pos]);
                newMedoids.Add(c);

                //new list of medoids are added to previous list
                medoidCollection.Add(documentCollection[pos]);
            }
            return newMedoids;
        }

    }
}
