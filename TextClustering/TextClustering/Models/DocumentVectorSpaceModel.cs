﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TextClustering.Models
{
    public class DocumentVectorSpaceModel
    {
        //Content represents the document(or any other object) to be clustered
        public string Content { get; set; }
        //represents the tf*idf (weight) of  each document
        public float[] VectorSpace { get; set; }
    }
}
