﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TextClustering.Models
{
    public class OutputModel
    {
        public List<DocumentObjModel> KmeanCluster { get; set; }
        public List<DocumentObjModel> KmedoidCluster { get; set; }
        public float totalDistanceKmedoid { get; set; }
        public float totalMeanKmean { get; set; }
        public long watchKmedoid { get; set; }
        public long watchKmean { get; set; }
    }
}
