﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TextClustering.Models
{
    public class DocumentObjModel
    {
        public List<DocumentVectorSpaceModel> GroupedDocument { get; set; }
        public float TotalValue { get; set; }
    }
}
